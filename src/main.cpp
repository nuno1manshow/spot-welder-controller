#include <Arduino.h>

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <RotaryEncoder.h>

#define LED_PIN LED_BUILTIN

#define RELAY_PIN 7

#define ROTARY_SWITCH_PIN 2
#define FOOT_SWITCH_PIN 4


// Variables will change:
int ledState = LOW;             // ledState used to set the LED
int rotarySwitchState = 0;

int footSwitchState = 0;
boolean relayOn = false;
unsigned long relayStartMillis = 0;


// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0;        // will store last time LED was updated

#define DEFAULT_PULSE 1.0
#define DEFAULT_TIMER 500
#define MAX_TIMER 990
#define MAX_PULSE 10.0

// constants won't change:
double pulse = DEFAULT_PULSE;
long timer = DEFAULT_TIMER;

// Setup a RoraryEncoder for pins A2 and A3:
RotaryEncoder encoder(A2, A3);

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels

#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

void testdrawchar(void) {
  display.clearDisplay();

  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setCursor(0, 0);     // Start at top-left corner
  display.cp437(true);         // Use full 256 char 'Code Page 437' font

  // Not all the characters will fit on the display. This is normal.
  // Library will draw what it can and the rest will be clipped.
  for(int16_t i=0; i<256; i++) {
    if(i == '\n') display.write(' ');
    else          display.write(i);
  }

  display.display();
  delay(2000);
}

void setup() {
  // set the digital pin as output:
  pinMode(LED_PIN, OUTPUT);
  pinMode(RELAY_PIN, OUTPUT);

  pinMode(ROTARY_SWITCH_PIN, INPUT_PULLUP);
  pinMode(FOOT_SWITCH_PIN, INPUT_PULLUP);

  Serial.begin(57600);
  Serial.println("SimplePollRotator example for the RotaryEncoder library.");

  // You may have to modify the next 2 lines if using other pins than A2 and A3
  PCICR |= (1 << PCIE1);    // This enables Pin Change Interrupt 1 that covers the Analog input pins or Port C.
  PCMSK1 |= (1 << PCINT10) | (1 << PCINT11);  // This enables the interrupt for pin 2 and 3 of Port C.

  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

}

// The Interrupt Service Routine for Pin Change Interrupt 1
// This routine will only be called on any signal change on A2 and A3: exactly where we need to check.
ISR(PCINT1_vect) {
  encoder.tick(); // just call tick() to check the state.
}

#define OPTION_SET_TIMER 1
#define OPTION_SET_PULSE 2
int selectedOption = OPTION_SET_TIMER;

void updateDisplay() {

  display.clearDisplay();
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.cp437(true);         // Use full 256 char 'Code Page 437' font

  if (relayOn) {
    display.setTextSize(3);
    display.setCursor(50, 5);
    display.write("!!!");
  } else {
    display.setTextSize(2);      // Normal 1:1 pixel scale

    display.setTextSize(1);
    display.setCursor(90, 8);
    display.write("timer");

    display.setCursor(90, 25);
    display.write("pulse");

    if (selectedOption == OPTION_SET_TIMER) {
      display.setTextSize(1);  
      display.setCursor(10, 4);     // Start at top-left corner
      display.write('>');
    } else {
      display.setTextSize(1);  
      display.setCursor(10, 22);     // Start at top-left corner
      display.write('>');
    }

    display.setTextSize(2);  
    display.setCursor(20, 0);
    char buf2[32];
    sprintf(buf2, "%dms", timer);
    if (timer == 0) {
      display.write("OFF");
    } else {
      display.write(buf2);
    }

    display.setTextSize(2);  
    display.setCursor(20, 18);

    char str_temp[6];
    dtostrf(pulse, 3, 1, str_temp);
    char buf3[32];
    sprintf(buf3, "%s s", str_temp);
    if (pulse == 0) {
      display.write("OFF");  
    } else {
      display.write(buf3);
    }
  }
  

  display.display();

}


void loop() {
  
    updateDisplay();
  
  int curRotSwState = digitalRead(ROTARY_SWITCH_PIN);
  int curFootSwState = digitalRead(FOOT_SWITCH_PIN);

  if (curRotSwState != rotarySwitchState) {
   rotarySwitchState = curRotSwState;
    if (rotarySwitchState == LOW) {
      if (selectedOption == OPTION_SET_TIMER) {
        selectedOption = OPTION_SET_PULSE;
      } else {
        selectedOption = OPTION_SET_TIMER;
      }
    }
  }
  
  unsigned long currentMillis = millis();

  if (curFootSwState != footSwitchState) {
    footSwitchState = curFootSwState;
    if (footSwitchState == LOW) {
      relayOn = true;
      relayStartMillis = currentMillis;
    } else {
      relayOn = false;
    }
  }

  if (relayOn && timer != 0 && relayStartMillis + timer <= currentMillis) {
    // Timer over
    relayOn = false;
  }

  if (relayOn) {
    digitalWrite(RELAY_PIN, HIGH);
  } else {
    digitalWrite(RELAY_PIN, LOW);
  }


  if (currentMillis - previousMillis >= pulse) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }

    // set the LED with the ledState of the variable:
    digitalWrite(LED_PIN, ledState);
  }

  static int pos = 0;
  #define ROTARY_INCREMENT_INTERVAL_TIMER 10
  #define ROTARY_INCREMENT_INTERVAL_PULSE 0.5

  int newPos = encoder.getPosition();
  if (pos != newPos) {

    if (selectedOption == OPTION_SET_TIMER) {
      if (pos < newPos) {
        if (timer < MAX_TIMER) {
          timer += ROTARY_INCREMENT_INTERVAL_TIMER;
        }
      } else {
        if (timer > 0) {
          timer -= ROTARY_INCREMENT_INTERVAL_TIMER;
        }
      }
    } else {
      if (pos < newPos) {
        if (pulse < MAX_PULSE) {
          pulse += ROTARY_INCREMENT_INTERVAL_PULSE;
        }
      } else {
        if (pulse > 0.0) {
          pulse -= ROTARY_INCREMENT_INTERVAL_PULSE;
        }
      }
    }
    
    Serial.print(newPos);
    Serial.println();
    pos = newPos;

  }
  


}