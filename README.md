## Spot welding timer

This project uses an arduino with a rotary encoder and a 128x32 OLED display to control a simple spot welder.

The idea is that a foot switch is attached to one of the input pins to allow hands free control.
Once the button is pressed, the relay is turned on for the specified amount of time, precisely controlling the welding time to achieve consistent results.

The rotary encoder can be used to customize the two settings:
 - Timer: controls for how long the relay is turned on
 - Pulse: adds a pulse timer to allow multiple pulses with cooling intervals while the


### How to use the rotary encoder:

  - Click the button to select the settings you want to modify
  - Rotate the dial to change the setting up or down



